﻿using System;

namespace GameOfLive
{
    class Program
    {
        static void Main(string[] args)
        {
            Board board = new Board(20);
            for (;;)
            {
                Console.Clear();
                board.Run();
                Console.Read();
            }
        }
    }
    
    public class Board
    {

        public Board(int board_size)
        {
            board_size_ = board_size;
            board_ =  new int[board_size_+2, board_size_+2];
            Random RandomNumberGenerator = new Random();
            for (int i = 1; i<board_size_-1; i++)
            {
                for (int j = 1; j < board_size_-1; j++)
                {
                    board_[i, j] = RandomNumberGenerator.Next(0, 2);
                }
            }
        }
      
        public void Run()
        {
            Display();
            ChangeBoardState();
        }

        private void ChangeBoardState()
        {
            int[,] board2_ =  new int[board_size_+2, board_size_+2];
            for (int i = 1; i<board_size_-1; i++)
            {
                for (int j = 1; j < board_size_ - 1; j++)
                {
                    board2_[i,j] = calculateCellFate(i, j);
                }
            }

            board_ = board2_;
        }
        
        private void Display()
        {
            for (int i = 1; i<board_size_-1; i++)
            {
                for (int j = 1; j < board_size_-1; j++)
                {
                    if (board_[i, j] == 0)
                    {
                        Console.Write(' ');
                    }
                    else
                    {
                        Console.Write('X');
                    }
                    Console.Write(" ");
                }
                Console.Write("\n");
            }
        }
        
        private int calculateCellFate(int x, int y)
        {
            int neighCount = getNeighourCountForCEllWithPosition(x, y);
            
            if (neighCount < 2)
                return 0;
            if (neighCount >= 2 && neighCount <= 3 && board_[x,y]==1)
                return 1;
            if (neighCount == 3)
                return 1;
            if (neighCount > 3)
                return 0;
            return 0;
        }

        private int getNeighourCountForCEllWithPosition(int x, int y)
        {
            int neighbours = 0;

            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = y - 1; j <= y + 1; j++)
                {
                    if (board_[i, j] == 1)
                        neighbours++;
                }
            }

            if (board_[x, y] == 1)
                neighbours--;
            return neighbours;
        }
        
        private int board_size_;
        private int[,] board_;
    }
}